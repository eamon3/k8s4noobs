# K8S TEST
little k8s test, used this tutorial as a guide

note that on firefox you must change the config to visit localhost:6000

https://kubernetes.io/blog/2019/07/23/get-started-with-kubernetes-using-python/

### My Build Instructions:

#### Build my image:

```docker build -f Dockerfile -t hello-world:latest . ```

-f Dockerfile is unnecessary here, as build will look for Dockerfile by default

#### Test my image:

```docker run -p 5001:5000 hello-world ```

check localhost:5001

#### Deploy to kubernetes:
```
kc apply -f deployment.yml
```
```
service/hello-world-lb created
deployment.apps/hello-world created
```
```
kc get all
```
```
NAME                               READY   STATUS    RESTARTS   AGE
pod/hello-world-67d974b548-68rxt   0/1     Pending   0          34s
pod/hello-world-67d974b548-88t6f   1/1     Running   0          34s
pod/hello-world-67d974b548-fwbvr   1/1     Running   0          34s
pod/hello-world-67d974b548-xmdcm   1/1     Running   0          34s

NAME                     TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
service/hello-world-lb   LoadBalancer   10.106.141.201   localhost     6000:31238/TCP   34s
service/kubernetes       ClusterIP      10.96.0.1        <none>        443/TCP          7d6h

NAME                          READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/hello-world   3/4     4            3           34s

NAME                                     DESIRED   CURRENT   READY   AGE
replicaset.apps/hello-world-67d974b548   4         4         3       34s
```
check localhost:6000

