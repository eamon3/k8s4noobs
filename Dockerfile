FROM python:3.8

RUN mkdir /k8s4noobs
WORKDIR /k8s4noobs
ADD . /k8s4noobs/
RUN pip install -r requirements.txt

EXPOSE 5000
CMD ["python", "/k8s4noobs/main.py"]
